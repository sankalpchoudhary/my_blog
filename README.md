# Master Branch Status
[Production Blog Link](http://prodsankalpblog.s3-website-us-west-2.amazonaws.com/)

[![pipeline status](https://gitlab.com/sankalpchoudhary/my_blog/badges/master/pipeline.svg)](https://gitlab.com/sankalpchoudhary/my_blog/-/commits/master)

[![coverage report](https://gitlab.com/sankalpchoudhary/my_blog/badges/master/coverage.svg)](https://gitlab.com/sankalpchoudhary/my_blog/-/commits/master)

[![Latest Release](https://gitlab.com/sankalpchoudhary/my_blog/-/badges/release.svg)](https://gitlab.com/sankalpchoudhary/my_blog/-/releases)


# Stage Branch Status

[Stage Blog Link](http://stagesankalpblog.s3-website-us-west-2.amazonaws.com/)

[![pipeline status](https://gitlab.com/sankalpchoudhary/my_blog/badges/stage/pipeline.svg)](https://gitlab.com/sankalpchoudhary/my_blog/-/commits/stage)

[![coverage report](https://gitlab.com/sankalpchoudhary/my_blog/badges/stage/coverage.svg)](https://gitlab.com/sankalpchoudhary/my_blog/-/commits/stage)


[![Latest Release](https://gitlab.com/sankalpchoudhary/my_blog/-/badges/release.svg)](https://gitlab.com/sankalpchoudhary/my_blog/-/releases)