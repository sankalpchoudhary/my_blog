---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
# weight: 1
# aliases: ["/first"]
tags: []
categories: []
author: "Sankalp Choudhary"
# author: ["Me", "You"] # multiple authors
showToc: true
TocOpen: false
draft: false,
hidemeta: false
comments: true
description: "" 
canonicalURL: "https://canonical.url/to/page"
disableHLJS: true # to disable highlightjs
disableShare: false
disableHLJS: false
hideSummary: false
searchHidden: true
ShowReadingTime: true
ShowBreadCrumbs: true
ShowPostNavLinks: true
ShowWordCount: true
ShowRssButtonInSectionTermList: true
UseHugoToc: true
cover:
    image: "<image path/url>" # image path/url
    alt: "<alt text>" # alt text
    caption: "<text>" # display caption under cover
    relative: false # when using page bundles set this to true
    hidden: true # only hide on current single page
editPost:
    URL: "https://gitlab.com/sankalpchoudhary/my_blog/-/blob/master/content/posts/{{ replace .Name "-" " " | title }}.md"
    Text: "Suggest Changes" # edit text
    appendFilePath: true # to append file path to Edit link
---

