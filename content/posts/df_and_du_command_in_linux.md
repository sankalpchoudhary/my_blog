---
title: "Df and du command in linux"
date: 2022-06-23T22:14:58+05:30
# weight: 1
# aliases: ["/first"]
tags: []
categories: []
author: "Sankalp Choudhary"
# author: ["Me", "You"] # multiple authors
showToc: true
TocOpen: false
draft: false,
hidemeta: false
comments: true
description: "in this article i show you how you can use du and df command to boost your linux productivity" 
canonicalURL: "https://canonical.url/to/page"
disableHLJS: true # to disable highlightjs
disableShare: false
disableHLJS: false
hideSummary: false
searchHidden: true
ShowReadingTime: true
ShowBreadCrumbs: true
ShowPostNavLinks: true
ShowWordCount: true
ShowRssButtonInSectionTermList: true
UseHugoToc: true
cover:
    image: "<image path/url>" # image path/url
    alt: "<alt text>" # alt text
    caption: "<text>" # display caption under cover
    relative: false # when using page bundles set this to true
    hidden: true # only hide on current single page
editPost:
    URL: "https://gitlab.com/sankalpchoudhary/my_blog/-/blob/master/content/posts/df_and_du_command_in_linux.md"
    Text: "Suggest Changes" # edit text
    appendFilePath: true # to append file path to Edit link
---


# df and ds command in linux

## df Command

this command is useful in linux storage managemnet they give information about how much storage is used by which file directory 

> This show how much storage use by which mount point
```
sankalp@SankalpLenovo:~$ df
Filesystem     1K-blocks     Used Available Use% Mounted on
tmpfs             592244     2380    589864   1% /run
/dev/nvme0n1p8 114760836 19142372  89742704  18% /
tmpfs            2961204    97900   2863304   4% /dev/shm
tmpfs               5120        4      5116   1% /run/lock
/dev/nvme0n1p9 184515944 21876896 153193368  13% /home
/dev/nvme0n1p1    262144    36492    225652  14% /boot/efi
tmpfs             592240     2436    589804   1% /run/user/1000


```

> If you want to show in humen readable then modify by this 
```
df -h
```
```
sankalp@SankalpLenovo:~$ df -h
Filesystem      Size  Used Avail Use% Mounted on
tmpfs           579M  2.4M  577M   1% /run
/dev/nvme0n1p8  110G   19G   86G  18% /
tmpfs           2.9G   86M  2.8G   3% /dev/shm
tmpfs           5.0M  4.0K  5.0M   1% /run/lock
/dev/nvme0n1p9  176G   21G  147G  13% /home
/dev/nvme0n1p1  256M   36M  221M  14% /boot/efi
tmpfs           579M  2.4M  576M   1% /run/user/1000

```


> If you want to show Type of Filesystem then modify by this 
```
df -hT
```
```
sankalp@SankalpLenovo:~$ df -hT
Filesystem     Type   Size  Used Avail Use% Mounted on
tmpfs          tmpfs  579M  2.4M  577M   1% /run
/dev/nvme0n1p8 ext4   110G   19G   86G  18% /
tmpfs          tmpfs  2.9G   82M  2.8G   3% /dev/shm
tmpfs          tmpfs  5.0M  4.0K  5.0M   1% /run/lock
/dev/nvme0n1p9 ext4   176G   21G  147G  13% /home
/dev/nvme0n1p1 vfat   256M   36M  221M  14% /boot/efi
tmpfs          tmpfs  579M  2.4M  576M   1% /run/user/1000
```

> If you want to show exclude some of Filesystem then modify by this 
```
df -hTx tmpfs
```
this remove tmpfs filesystem
```
sankalp@SankalpLenovo:~$ df -hTx tmpfs
Filesystem     Type  Size  Used Avail Use% Mounted on
/dev/nvme0n1p8 ext4  110G   19G   86G  18% /
/dev/nvme0n1p9 ext4  176G   21G  147G  13% /home
/dev/nvme0n1p1 vfat  256M   36M  221M  14% /boot/efi

```

## du Command

This command is useful in indivisual file directory stoage anylysis

> This command show you how much storage used by your files in directory and whole directory 
```
du -h  /home/sankalp 
```
but it is little bit ugly in terminal 
```
12K	/etc/firefox
8.0K	/etc/sgml/docbook-xml/4.1.2
8.0K	/etc/sgml/docbook-xml/4.4
8.0K	/etc/sgml/docbook-xml/4.3
8.0K	/etc/sgml/docbook-xml/4.2
8.0K	/etc/sgml/docbook-xml/4.0
8.0K	/etc/sgml/docbook-xml/4.5
52K	/etc/sgml/docbook-xml
252K	/etc/xdg
8.0K	/etc/hp
12K	/etc/udisks2
4.0K	/etc/vulkan/icd.d
52K	/etc/kernel
8.0K	/etc/python3
17M	/etc

...
...
...
...
```

> you can use  max-depth argument to beautify result
 ```
du -h --max-depth 1 /home/sankalp
```
```
sankalp@SankalpLenovo:~$ du -h --max-depth 1 /home/sankalp 
768M	/home/sankalp/Downloads
468M	/home/sankalp/code
403M	/home/sankalp/.npm
12K	/home/sankalp/.dart
51M	/home/sankalp/.dartServer
1.4G	/home/sankalp/.gradle
5.0G	/home/sankalp/Android
2.3M	/home/sankalp/.quokka
882M	/home/sankalp/.local
562M	/home/sankalp/.vscode
16K	/home/sankalp/.mozilla
76K	/home/sankalp/.pki
4.0K	/home/sankalp/.wallaby
21G	/home/sankalp
1.4G	/home/sankalp/.config
104K	/home/sankalp/Templates
8.0K	/home/sankalp/.yarn
76K	/home/sankalp/.pki
4.0K	/home/sankalp/.wallaby
21G	/home/sankalp
...
...
...


```
> If you add "s" in this command it become more humen readable
```
du -hs  /home
```
```
sankalp@SankalpLenovo:~$ sudo du -hs  /home
21G	/home


```
> You can also chain directory in this command

```
sudo du -hs /home/sankalp /etc
```
```
21G	/home/sankalp
17M	/etc

```

> If you want to total all storage then you can use this

```
sudo du -hsc  /home/sankalp /etc

```
It gives total ammount of stoarge used by both directory
```
sankalp@SankalpLenovo:~$ sudo du -hsc  /home/sankalp /etc
21G	/home/sankalp
17M	/etc
21G	total


```
