---
title: "Fix on save file folder selection remove"
date: 2022-06-09T11:27:15Z
# weight: 1
# aliases: ["/first"]
tags: ["Time_Saver", "Linux", "Quick Tips"]
categories: ['']
author: "Sankalp Choudhary"
# author: ["Me", "You"] # multiple authors
showToc: true
TocOpen: false
draft: false,
hidemeta: false
comments: true
description: "Desc Text." 
canonicalURL: "https://canonical.url/to/page"
disableHLJS: true # to disable highlightjs
disableShare: false
disableHLJS: false
hideSummary: false
searchHidden: true
ShowReadingTime: true
ShowBreadCrumbs: true
ShowPostNavLinks: true
ShowWordCount: true
ShowRssButtonInSectionTermList: true
UseHugoToc: true
cover:
    image: "<image path/url>" # image path/url
    alt: "<alt text>" # alt text
    caption: "<text>" # display caption under cover
    relative: false # when using page bundles set this to true
    hidden: true # only hide on current single page
editPost:
    URL: "https://github.com/<path_to_repo>/content"
    Text: "Suggest Changes" # edit text
    appendFilePath: true # to append file path to Edit link
---
By clicking on selected folder in GtkFileChooser doesn't deselect the folder.

If you want to deselect the selected folder, instead of simply clicking on the selected folder, click while pressing the Ctrl key.

