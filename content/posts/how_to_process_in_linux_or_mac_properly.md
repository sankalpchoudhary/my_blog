---
title: "How to kill process or port in linux or mac properly"
date: 2022-06-11T20:00:12+05:30
# weight: 1
# aliases: ["/first"]
tags: ["CI/CD", "Hugo", "Blog", "AWS", ]
categories: ['CI/CD', "AWS"]
author: "Sankalp Choudhary"
# author: ["Me", "You"] # multiple authors
showToc: true
TocOpen: false
draft: false,
hidemeta: false
comments: true
description: "We all face problem in our linux server or in our ubunutu dev env taht we have listing service on our desired port we have some solution for that" 
canonicalURL: "https://canonical.url/to/page"
disableHLJS: true # to disable highlightjs
disableShare: false
disableHLJS: false
hideSummary: false
searchHidden: true
ShowReadingTime: true
ShowBreadCrumbs: true
ShowPostNavLinks: true
ShowWordCount: true
ShowRssButtonInSectionTermList: true
UseHugoToc: true
cover:
    image: "<image path/url>" # image path/url
    alt: "<alt text>" # alt text
    caption: "<text>" # display caption under cover
    relative: false # when using page bundles set this to true
    hidden: true # only hide on current single page
editPost:
    URL: "https://github.com/<path_to_repo>/content"
    Text: "Suggest Changes" # edit text
    appendFilePath: true # to append file path to Edit link
---


<!-- ##### In Linux or Macs when we start some program in after closing it we have issue that some program is  is already listen on this port so how we close this port -->

# I write some command to dignose this problem

### This command is useful for know which service is running on specific port

```lsof -i :<port_number>```

eg:
```
lsof -i :8080 
```

### If we Know that this service occupy by which service then we move forward

now if we want to know PID of service then we have this command

```
pgrep -f <some info related to process>
```

eg:
```
pgrep -f 'hugo serve -D'
```
```
pgrep -f 'nodemon app.js'
```
```
it returs process id like 25685

```

now we get process id by this now we will kill the program by 

```
kill <process_id>
```

we have pipeline both command this command and we have final command in result

```
kill $(pgrep -f 'hugo serve -D')
```
```
kill $(pgrep -f 'nodemon app.js')
```
```
kill $(pgrap -f 'nodemon /bin/www')
```