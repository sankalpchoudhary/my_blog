---
title: "Setup Hugo Theme for your Personal Blog with CI/CD"
date:  2022-06-04T11:55:40Z
# weight: 1
# aliases: ["/first"]
tags: ["CI/CD", "Hugo", "Blog", "AWS", ]
categories: ['CI/CD', "AWS"]
author: "Sankalp Choudhary"
# author: ["Me", "You"] # multiple authors
showToc: true
TocOpen: false
draft: false,
hidemeta: false
comments: true
description: "Desc Text." 
canonicalURL: "https://canonical.url/to/page"
disableHLJS: true # to disable highlightjs
disableShare: false
disableHLJS: false
hideSummary: false
searchHidden: true
ShowReadingTime: true
ShowBreadCrumbs: true
ShowPostNavLinks: true
ShowWordCount: true
ShowRssButtonInSectionTermList: true
UseHugoToc: true
cover:
    image: "<image path/url>" # image path/url
    alt: "<alt text>" # alt text
    caption: "<text>" # display caption under cover
    relative: false # when using page bundles set this to true
    hidden: true # only hide on current single page
editPost:
    URL: "https://github.com/<path_to_repo>/content"
    Text: "Suggest Changes" # edit text
    appendFilePath: true # to append file path to Edit link
---

## Setup Hugo for your Personal Blog with CI/CD

| Tech Stack      | Description |
| ----------- | ----------- |
| AWS S3      | We use aws S3 because it provide us checp hosting plus if we get more number of user than we do not worry about scaling AWS Mange it       |
| Gitllab   | We Use gitlab for storing our rapo and setup up CI/CD Pipline for it        
| Hugo   | We Use Hugo for generating our stating site |

##This is Code we use it to deploy it to AWS S3

```
# Set the default container image to GitLab's Hugo image
image: registry.gitlab.com/pages/hugo:latest

variables:
  # Ensure the git submodules are updated when the pipeline runs
  GIT_SUBMODULE_STRATEGY: recursive
  
stages:
  - build
  - deploy

# Build the Staging site
buildStaging:
  stage: build
  script:
    # Run hugo with a different baseURL to ensure links work in the staging site, and flag to include drafts and future posts
    - echo http://$AWS_S3_Staging_Bucket.s3-website.$AWS_DEFAULT_REGION.amazonaws.com/
    - hugo --minify --baseURL http://$AWS_S3_Staging_Bucket.s3-website.$AWS_DEFAULT_REGION.amazonaws.com/ --buildDrafts --buildFuture
  artifacts:
    paths:
      # Create an artifact folder of the hugo "public" output
      - public
  only:
  - stage

# Deploy the Staging site
deployStaging:
  # In the deploy stage
  stage: deploy
  # Use a docker image with AWS CLI pre-installed
  image: garland/aws-cli-docker
  script:
  # Synchronise the "public" folder passed from buildStaging to the Staging AWS S3 bucket name from the variable
  - aws s3 sync ./public s3://$AWS_S3_Staging_Bucket --delete --only-show-errors
  dependencies:
  # Depend on the buildStaging job to ensure the "public" artifact is available
  - buildStaging
  only:
  - stage


# Build the Production site
buildProduction:
  # In the build stage
  stage: build 
  script:
    # Run hugo with just the minify command (no drafts or future posts)
    - hugo --minify 
  artifacts:
    paths:
      # Create an artifact folder of the hugo "public" output
      - public

  only:
  - master
# Deploy the Production site
deployProduction:
  # In the deploy stage
  stage: deploy
  # Use a docker image with AWS CLI pre-installed
  image: garland/aws-cli-docker
  script:
  # Synchronise the "public" folder passed from buildProduction to the Production AWS S3 bucket name from the variable
  - aws s3 sync ./public s3://$AWS_S3_Production_Bucket --delete --only-show-errors
  # Invalidate the cloudfront cache to make the changes live
  # - aws cloudfront create-invalidation --distribution-id E37FU6DHWYT5T2 --paths "/*"

  dependencies:
  # Depend on the buildProduction job to ensure the "public" artifact is available
  - buildProduction

  only:
  - master
```

I show you how you can upload this blog post
